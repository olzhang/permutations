import java.io.*;
import java.util.*;
import java.lang.Math.*;


public class Permute {
	/**
	 * finds all the permutations of a string in ~n! space and ~n! time 
	 * v. good for stress testing
	 * compile: javac Permute.java
	 * run: java Permute
	 * usage:
	 * input a sequence of chars
	 * now watch your computer suffer ahahaha 
	 */
	
	public static LinkedList<StringBuilder> permute(StringBuilder str) {

		LinkedList<StringBuilder> allPermutes = new LinkedList<>();
		for (int i=0; i<str.length(); i++) {
			StringBuilder remainingChars = new StringBuilder(str);
			StringBuilder currentChar = new StringBuilder();
			currentChar.append(str.charAt(i));
			permutHelper(currentChar, remainingChars.deleteCharAt(i), allPermutes);
		}	
		return allPermutes;
	}

	public static void permutHelper(StringBuilder resultSoFar, StringBuilder remainder,
		LinkedList<StringBuilder> results) {

		if (remainder.length() == 0) {
			results.add(resultSoFar);
			return;
		}
		for (int i=0; i<remainder.length(); i++) {
			StringBuilder nextResultSoFar = new StringBuilder(resultSoFar);
			nextResultSoFar.append(remainder.charAt(i));
			StringBuilder nextRemainders = new StringBuilder(remainder);
			permutHelper(nextResultSoFar, nextRemainders.deleteCharAt(i), results);
		} 

	}
	
	// main function for demo puposes 
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a sequence of chars: ");
		String input = reader.next(); 
		StringBuilder s = new StringBuilder(input);
		LinkedList<StringBuilder> results = permute(s);
		int outcome = 0;

		System.out.println("output: \n");
		for (StringBuilder i : results) {
			System.out.print(++outcome);
			System.out.print(": ");
			System.out.println(i.toString());
		} 
	}

}




